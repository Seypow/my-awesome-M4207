## PROJET'S DESCRIPTION
The project is: RT Key Managment. You have to manage the keys of the R&T department.
This means that :


At anytime, it should be possible to localize the keys. (We assume that they stay within the IUT campus)
If a key battery is to low, it should raise an alert. (You choose the alert)
We should be able to know who borrowed the keys (use your imagination, implements some mechanisms).
Keys are also a way to monitor the RT departement with other sensors (cooling system, ...) 
Key states (localisation, sensor states, ...) should be easily accessible.

## HARDWARES AND SOFTWARES
LinkIt ONE
Buzzer
baseshield
gps
antenne wifi
grove-LCD
battery
arduino

## TO DO
we plug the gps and the antenna for the wifi
then we plug the grove lcd on the baseshield on the pin I2C
The Buzzer on the pin D2
then we created the program and we upload it in the Linkit One

## GROUP
Olivier PAUSE, Annas REALE, Léo GEREONE
 
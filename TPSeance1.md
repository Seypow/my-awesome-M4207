Pour cette séance de TP j'ai choisi de m'occuper de ce qui me semble le plus facile:
    - la signalisation quand la batterie est faible.
    
    
Cette étape n'est présente pas de grande difficulté car les codes sont fourni dans les exemples du lociel Arduino.
J'ai pris les codes ''Blink'' et ''Battery'' et les ai mélangé. 
J'ai ajouté une boucle if pour qu'un pin clignote quand la batterie est faible. 
Le code en question est appelé batteriefaible:
Pour ma part, je pense qu'on peut amélioré le tout en ajoutant la led rouge pour que ce soit plus visible et le ''bip'' (je sais pas le nom) pour avoir une 
signalisation sonore en plus.


Pour la prochaine séance, je pense que mon partenaire devrait soit terminer la signalisation en ajoutant ce que jai cité plus haut ou se pencher sur le tracege de la clé
car j'ai l'impression qu'utilisé un module gps n'est pas suffisant pour déterminer avec précision la position de la clé. Il devrait faire des test supplémentaires pour en 
être certain.



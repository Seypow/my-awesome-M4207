#include <SPI.h>
#include <LTask.h>
#include <LWiFi.h>
#include <LWiFiClient.h>

#define WIFI_AP "RT-WIFI-Guest"
#define WIFI_PASSWORD "wifirtguest"
#define WIFI_AUTH LWIFI_WPA  // choose from LWIFI_OPEN, LWIFI_WPA, or LWIFI_WEP.

void setup() {
  //Initialize serial and wait for port to open:
  LTask.begin();
  LWiFi.begin();
  Serial.begin(9600);
    // keep retrying until connected to AP
    Serial.println("Connecting to AP");
    while (0 == LWiFi.connect(WIFI_AP, LWiFiLoginInfo(WIFI_AUTH, WIFI_PASSWORD)))
    {
      delay(1000);
    }
  }

  // I plan to use this function in the future to reconnect to WiFi if the connection drops:
  boolean wifi_status(LWifiStatus ws){
    switch(ws){
      case LWIFI_STATUS_DISABLED:
        return false;
      break;
      case LWIFI_STATUS_DISCONNECTED:
        return false;
      break;
      case LWIFI_STATUS_CONNECTED:
        return true;
      break;
    }
    return false;
  }

void loop() {
  Serial.println("Connected!");
  printMacAddress();
    while(1){
      LWifiStatus ws = LWiFi.status();
      boolean status = wifi_status(ws);
  
      if(!status){
        Serial.println("Connecting to AP");
        while (0 == LWiFi.connect(WIFI_AP, LWiFiLoginInfo(WIFI_AUTH, WIFI_PASSWORD)))
        {
          delay(500);
        }
      }
    }
}

void printMacAddress(){
  byte bssid[6];
    LWiFi.BSSID(bssid);    
    Serial.print(" SSID : ");
    Serial.print(bssid[5],HEX);
    Serial.print(":");
    Serial.print(bssid[4],HEX);
    Serial.print(":");
    Serial.print(bssid[3],HEX);
    Serial.print(":");
    Serial.print(bssid[2],HEX);
    Serial.print(":");
    Serial.print(bssid[1],HEX);
    Serial.print(":");
    Serial.println(bssid[0],HEX);
}


